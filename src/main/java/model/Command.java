package model;

public interface Command {

    void command(String arg);

}
