package model;

@FunctionalInterface
public interface Function {

    int exec(int a, int b, int c);

}
