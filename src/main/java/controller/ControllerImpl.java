package controller;

import model.Command;
import model.Function;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import view.*;

public class ControllerImpl implements Controller {

    private static final Logger logger = LogManager.getLogger();

    private View view;

    private CommandImpl commandImpl = new CommandImpl();

    public ControllerImpl() {
        view = new ViewImpl();
    }

    @Override
    public void start() {
        Menu tasks = new Menu(Integer.MAX_VALUE);
        tasks.addMenuOption("1", new Option("Task 1", () -> {
            Function max = (a, b, c) -> Math.max(a, Math.max(b, c));
            Function avg = (a, b, c) -> (a + b + c) / 3;

            logger.debug(max.exec(2, 4, 8));
            logger.debug(avg.exec(3, 3, 3));
        }));

        tasks.addMenuOption("2", new Option("Task 2", () -> {
            Command lambda = logger::debug;

            Command object = new Command() {
                @Override
                public void command(String arg) {
                    logger.debug(arg);
                }
            };

            Menu subTasks = new Menu(tasks, 4);

            subTasks.addMenuOption("lambda", new Option("Command implemented by lambda expression", () -> lambda.command("Lambda command")));
            subTasks.addMenuOption("ref", new Option("Command implemented by method reference", () -> {
                view.userInput("Enter a message: ", commandImpl::command);
            }));
            subTasks.addMenuOption("anonymous", new Option("Command implemented by anonymous class", () -> new Command() {
                @Override
                public void command(String arg) {
                    logger.debug(arg);
            }
            }));
            subTasks.addMenuOption("instance", new Option("Command implemented by object instance", () -> object.command("Object command")));

            view.setMenu(subTasks);
        }));

        view.setMenu(tasks);

        view.showMenu();
    }

}
