package view;

import model.Command;

public interface View {

    void setMenu(Menu menu);

    void showMenu();

    String userInput(String message);

    void userInput(String message, Command command);

    void waitForUserOption();

    boolean doAction(String key);

}
