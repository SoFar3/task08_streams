package view;

import model.Command;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CommandImpl implements Command {

    private static final Logger logger = LogManager.getLogger();

    @Override
    public void command(String arg) {
        logger.debug(arg);
    }

}
