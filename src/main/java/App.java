import controller.Controller;
import controller.ControllerImpl;

public class App {

    public static void main(String[] args) {
        Controller controller = new ControllerImpl();
        controller.start();
    }

}